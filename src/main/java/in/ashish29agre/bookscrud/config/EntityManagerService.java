/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.bookscrud.config;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Singleton;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.EntityManager;
import javax.ws.rs.Path;
import org.jvnet.hk2.annotations.Service;

/**
 *
 * @author Ashish
 */
@Service
@Singleton
@Path("/dbService")
public class EntityManagerService {

    private static final String PERSISTENT_UNIT_NAME = "in.ashish29agre_BooksCRUD_war_1.0PU";
    private final static Logger logger = Logger.getLogger(EntityManagerService.class.getSimpleName());
    private final EntityManagerFactory emf;
    private final EntityManager eManager;

    public EntityManagerService() {
        emf = Persistence.createEntityManagerFactory(PERSISTENT_UNIT_NAME);
        eManager = emf.createEntityManager();
        logger.log(Level.FINEST, "Initialized entity manager");
    }
    
    public EntityManager getEntityManager() {
        return this.eManager;
    }
}
